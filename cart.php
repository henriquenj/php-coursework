<?php

require("header.php");
require_once("database.php");
echo "<br><br><br><br><br><br>";

// not logged? goes to login page
if ($user == NULL)
	{
		header('Location:login.php');
	}

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		if (isset($_POST['removedItem']))
			{
				// an item was removed
				//..and we just got the position inside the 'cart' array
				unset($_SESSION['cart'][$_POST['removedItem']]);
				$_SESSION['cart'] = array_values($_SESSION['cart']);
				// decrement counter
				$_SESSION['itensAmount'] -= 1;
				if ($_SESSION['itensAmount'] == 0)
					{
						unset($_SESSION['itensAmount']);
						unset($_SESSION['cart']);
					}
			}
		else
			{
				$isOnCart = false;
				// number of itens in the cart
				//...act as a counter
				if (isset($_SESSION['itensAmount']))
					{
						$_SESSION['itensAmount'] += 1;
						// check if this item was already added to the cart
					
						for($b = 0; $b < $_SESSION['itensAmount']-1; $b++)
							{
								// check if the iten is on the cart
								if ($_SESSION['cart'][$b] == $_POST['dvd'])
									{
										$isOnCart = true;
										break;
									}
							}
					
					}
				else
					{
						$_SESSION['itensAmount'] = 1;
					}

				if (!$isOnCart)
					{
						//... this is the ID of the dvd
						$dvd = $_POST['dvd'];
						$_SESSION['cart'][$_SESSION['itensAmount']-1] = $dvd;//add to the cart list
					}
			}
	}

// print beggining of product list
echo "<br><br><br><div class=\"row\"><center><div class=\"col-md-6 center-block\"><div class=\"alert alert-success\">";

$con = DatabaseConnect();
if (isset($_SESSION['itensAmount']))
	{
		// print top part of the table
		echo "<table class=\"table table-bordered\">
<tr>
<td><b>Title</b></td>
<td><b>Price</b></td>
<td></td>
</tr>
";
        //query the info about the itens

        // build query
		$query = "SELECT DVD_Title,Price FROM dvdlist WHERE ";
		foreach ($_SESSION['cart']  as $iten)
			{
				$query .= "id = $iten OR ";
			}
		$query = substr($query, 0, -3); // remove last OR
		$query .= ';';
        //print the list of itens
		$result = mysqli_query($con,$query);
		$rows =  mysqli_num_rows($result);
		$totalPrice = 0; // to be shown at the end of the table
		for($a = 0; $a < $rows; $a++)
			{
				$obj = mysqli_fetch_object($result);
				$totalPrice += $obj->Price;
				// build talble
				echo"
<tr>
<td>$obj->DVD_Title</td>
<td>£$obj->Price</td>
<form action=\"cart.php\" method=\"post\">
<td><button class=\"btn btn-info\" value=$a name=\"removedItem\" >Remove from cart</button></form></td>
</tr>
";
			}
		// finish table
		echo "</table><b>Total: £$totalPrice</b>";
	}
else
	{
		echo "There's no itens in your cart, why don't you add some?<br><br>";
	}

// final part of html
echo'
</div></div><div><a type="button" class="btn btn-info" href="index.php">Back to the list</a>  <a type="button" class="btn btn-warning" href="empty.php">Erase Cart</a>  <a type="button" class="btn btn-success" href="checkout.php">Checkout</a></center></div>
';


DatabaseDisconnect($con);
require("footer.php");

?>
