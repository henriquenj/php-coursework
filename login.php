<?php

require("header.php");

// already logged users goes to index
if ($user != NULL)
	{
		// we are logged, go to index page
		header('Location:index.php');
	}

// every user that is not logged is redirected to the login page by default
// this script handles sessions and logs the user

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		require_once("database.php");
		// html for draw the errors
		$finalWarning ="<br><br><br><br><br><br><center><div class=\"row \"><div class=\"col-md-6 center-block \"><div class=\"alert alert-danger\">";
		$error = false;

		$con = DatabaseConnect();


		// copy to local variables
		$username = $_POST['username'];
		$password = $_POST['password'];

		if (empty($username))
			{
				$finalWarning .= "You must type a username!<br>";
				$error = true;
			}
		if (empty($password))
			{
				$finalWarning .= "You must type a password!<br>";
				$error = true;
			}
        //check typed data against the database
		if (!$error)
			{
				// hash the password using the username as salt
				$hash = hash("sha256",$password . $username);
				// check if the user exist
				$result = mysqli_query($con,"SELECT hash FROM users WHERE username = '$username';");
				$num = mysqli_num_rows($result);
				if ($num == 0)
					{
						$finalWarning .= "Username and password don't match!<br>";
						// no user
						$error = true;
					}
				else // found one user
					{
						// compare new hash with database hash
						$obj = mysqli_fetch_object($result);
						$storedHash = $obj->hash;
						if ($storedHash != $hash)
							{
								// wrong password
								$finalWarning .= "Username and password don't match!<br>";
								$error = true;								
							}
					}
			}

		if ($error)
			{
                // put final piece
				$finalWarning .= "<br><a type=\"button\" class=\"btn btn-danger\" href=\"login.php\">Go back</a></div></div><br></center></div>";
				// print error composed error message
				echo "$finalWarning";
			}
		else
			{
				
				//see when was the last time the user logged in
				$qData = mysqli_query($con,"SELECT lastLogin FROM users WHERE username = '$username'");
				$date = mysqli_fetch_object($qData);

				// we are logged, no errors
				session_start();
				$sessionID = session_id();
				// store session on the database
				mysqli_query($con,"UPDATE users SET sessionID = '$sessionID' WHERE username = '$username'");
				session_destroy();

				// print confirmation
				echo "<br><br><br><br><br><br><div class=\"row\"><center><div class=\"col-md-6 center-block\"><div class=\"alert alert-success\">Success, welcome back $username. Your last visit was on $date->lastLogin<br><br><a type=\"button\" class=\"btn btn-success\" href=\"index.php\">Continue</a>
</div></div><div></center>";
			}

		DatabaseDisconnect($con);
	}

else
	{
        //output html form
		// heavily modified from this template 
		// http://bootsnipp.com/snippets/featured/parallel-signin-and-signup
		echo '
<br><br><br><br>
<center>
  <form class="form-horizontal" action="login.php" method="post">
    <div class="heading">
      <h4 class="form-heading">Sign In</h4>
    </div>

      <div class="control-group">
	<label class="control-label" for=
               "inputUsername">Username</label>

	<div class="controls">
          <input id="inputUsername" placeholder=
		 "E.g. henry" type="text" name="username" required >
          </div>
	</div>

	<div class="control-group">
          <label class="control-label" for=
		 "inputPassword">Password</label>

          <div class="controls">
            <input id="inputPassword" placeholder=
                   "Min. 4 Characters" type="password" name="password" required>
            </div>
          </div>

          <div class="controls">
	     <br> <button class="btn btn-success" type="submit" name="submit">Sign In</button>
	       <a type="button" class="btn btn-default" href="signup.php">Create Account</a>
	    </div>
          </form>

	  </center>

';
	}


require("footer.php");

?>
