
<?php

// source for database related funcions


// connect to the local database, returning the connection if successful
function DatabaseConnect()
{
	// TODO: replace with igor databse login information
	$con = mysqli_connect("localhost","admin","aspas");
	// capture errors
	if (mysqli_connect_errno())
		{
			echo "Connection to the database failed, please contact the administrator at henriquenj@gmail.com.";
			return NULL;
		}

	if (!mysqli_select_db($con,"moviesStore"))
		{
			echo "Couldn't select the database, please contact the administrator at henriquenj@gmail.com.";
			return NULL;
		}

	return $con;
	
}

// Disconnect from the database
function DatabaseDisconnect(mysqli $con)
{
	mysqli_close($con);
}

?>