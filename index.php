<?php


require("header.php");


// not logged? goes to login page
if ($user == NULL)
	{
		header('Location:login.php');
	}

$rowsPerPage = 100; // default value
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		if (isset($_POST['currentPage']))
			{
				$currentPage = $_POST['currentPage'];	 
				$_SESSION['currentPage'] = $currentPage; // store current page on the session
			}

	}
//set default page or restore the previously one (useful if the user is comming back from the cart)
else if (!isset($_SESSION['currentPage']))
	{
		$currentPage = 0;
		$_SESSION['currentPage'] = 0;
	}
else
	{
		$currentPage = $_SESSION['currentPage'];
	}

echo'
<br><br><br><br><br>
	  <a type="button" class="btn btn-default" href="sell.php">Sell</a>
<a type="button" class="btn btn-info" href="cart.php">Cart</a>
<br><br>
';

$con = DatabaseConnect();

//query number of itens
$amountItensQuery = mysqli_query($con,"SELECT COUNT(*) FROM dvdlist");
$totalAmountArray = mysqli_fetch_assoc($amountItensQuery);
$totalAmount = $totalAmountArray['COUNT(*)'];

// page menu
// calculate number of pages
$pagesNumber = ceil($totalAmount / $rowsPerPage);

echo '<form class="form-vertical" action="index.php" method="post">';
for ($a = 0; $a < $pagesNumber; $a++)
	{
		if ($currentPage == $a)
			{
				echo "<button class=\"btn\ btn-warning\" type=\"submit\" name=currentPage value=$a>$a </button>";
			}
		else
			{
				echo "<button class=\"btn\ btn-info\" type=\"submit\" name=currentPage value=$a>$a </button>";
			}
	}

echo '</form>';
$offset = $currentPage * $rowsPerPage;
$result = mysqli_query($con,"SELECT * FROM dvdlist ORDER BY DVD_Title LIMIT $rowsPerPage OFFSET $offset; ");
$rows = mysqli_num_rows($result);

// print first row
echo '
<br>
<table class="table table-bordered">
  <tr>
    <td><b>Owner</b></td>
    <td><b>Title</b></td>
    <td><b>Studio</b></td>
    <td><b>Status</b></td>
    <td><b>Sound</b></td>
    <td><b>Price</b></td>
    <td><b>Rating</b></td>
    <td><b>Year</b></td>
    <td><b>Genre</b></td>
    <td><b>Aspect</b></td>
    <td><b>UPC</b></td>
    <td><b>DVD Release Date</b></td>
    <td><b>Timestamp</b></td>
  </tr>

';

// print all rows of the table
for ($a = 0; $a < $rows; $a++)
	{
		$obj = mysqli_fetch_object($result);		
		$UPC = number_format($obj->UPC,2);
		$isOnCart = false;
		if (isset($_SESSION['itensAmount']))
			{
				for($b = 0; $b < $_SESSION['itensAmount']; $b++)
					{
						// check if the iten is on the cart
						if ($_SESSION['cart'][$b] == $obj->ID)
							{
								$isOnCart = true;
								break;
							}
					}
			}
		echo"
<tr>
    <td>$obj->username</td>
    <td>$obj->DVD_Title</td>
    <td>$obj->Studio</td>
    <td>$obj->Status</td>
    <td>$obj->Sound</td>
    <td>£$obj->Price</td>
    <td>$obj->Rating</td>
    <td>$obj->Year</td>
    <td>$obj->Genre</td>
    <td>$obj->Aspect</td>
    <td>$UPC</td>
    <td>$obj->DVD_ReleaseDate</td>
    <td>$obj->Timestamp</td>
    <form action=\"cart.php\" method=\"post\">
";
		// see if the user already have this item
		if($user == $obj->username)
			{
				echo "<td>This item is already yours</td>";
			}
		else if ($isOnCart)
				{
					// remove button from the interface
					echo"<td>Already on cart</td>";
				}
			else
				{
					echo" <td><button class=\"btn btn-info\" value=$obj->ID name=\"dvd\">Add to cart</button></td>";
				}

		echo "
    </form>
</tr>
";
	}

// finish table
echo '</table><center>';


DatabaseDisconnect($con);
require("footer.php");

?>
