<?php

require("header.php");
// not logged? goes to login page
if ($user == NULL)
	{
		header('Location:login.php');
	}

// the only purpose of this script is to erase the cart

// remove the cart from the session
unset($_SESSION['itensAmount']);
unset($_SESSION['cart']);

//.. and finally goes back to the cart
header('Location:cart.php');

?>