

<?php

require("header.php");
require_once("database.php");
// not logged? goes to login page
if ($user == NULL)
	{
		header('Location:login.php');
	}

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
        // html for draw the errors
		$finalWarning ="<br><br><br><br><br><br><center><div class=\"row \"><div class=\"col-md-6 center-block \"><div class=\"alert alert-danger\">";
		
		$error = false;
		$con = DatabaseConnect();
		

		// copy to local variables
		$price = $_POST['price'];
		$title = $_POST['title'];
		$studio = $_POST['studio'];
		$sound = $_POST['sound'];
		$versions = $_POST['versions'];
		$rating = $_POST['rating'];
		$year = $_POST['year'];
		$aspect = $_POST['aspect'];
		$upc = $_POST['upc'];
		$dvdRelease = $_POST['dvdRelease'];


		if (empty($name))
			{
				$finalWarning .= "You must provide a name for your product";
			}

		if (empty($price))
			{
				$finalWarning .= "You must provide a price for your product!<br>";
				$error = true;
			}
		
		if ($error)
			{
				$finalWarning .= "<br><a type=\"button\" class=\"btn btn-danger\" href=\"sell.php\">Go back</a></div></div><br></center></div>";
				// print composed error message
				echo "$finalWarning";
			}
		else
			{
				// no errors, put everything in te database
				echo "<br><br><br><br><br><br><div class=\"row\"><center><div class=\"col-md-6 center-block\"><div class=\"alert alert-success\">You are now selling $title<br><br><a type=\"button\" class=\"btn btn-success\" href=\"index.php\">Continue</a>
</div></div><div></center>";

				mysqli_query($con,"INSERT INTO dvdlist (Price,DVD_Title,Studio,Sound,Versions,Rating,Year,Aspect,UPC,DVD_ReleaseDate) VALUES ('$price','$title','$studio','$sound','$versions','$rating','$year','$aspect','$upc','$dvdRelease');");
				mysqli_error($con);
			}


		DatabaseDisconnect($con);
	}

else
	{

echo '
<br><br><br><br>
<center>
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="span6">
          <div class="area">
            <form class="form-horizontal" action="sell.php" method="post">
              <div class="heading">
                <h4 class="form-heading">Add new product</h4>
              </div>

              <div class="control-group">
                <label class="control-label" for=
                       "inputUser">Title*</label>

                <div class="controls">
                  <input id="inputUser" placeholder=
                         "E.g. Lord of the Rings" type="text" name="title" required>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label" for=
			 "inputPassword">Price*</label>

                  <div class="controls">
                    <input id="inputPassword" placeholder=
                           "E.g. 19.90" type="number" name="price" required>
                    </div>
                  </div>

			<div class="control-group">
			  <label class="control-label" for=
				 "inputUser">Studio</label>

			  <div class="controls">
			    <input id="inputUser" type="text" name="studio">
			    </div>
			  </div>


			  <div class="control-group">
			    <label class="control-label" for=
				   "inputUser">Status</label>

			    <div class="controls">
			      <input id="inputUser" placeholder=
				     "E.g. Out" type="text" name="status">
			      </div>
			    </div>

			  <div class="control-group">
			    <label class="control-label" for=
				   "inputUser">Sound</label>

			    <div class="controls">
			      <input id="inputUser" placeholder=
				     "E.g. 5.1" type="text" name="sound">
			      </div>
			    </div>


			  <div class="control-group">
			    <label class="control-label" for=
				   "inputUser">Versions</label>

			    <div class="controls">
			      <input id="inputUser" placeholder=
				     "E.g. 04:03" type="text" name="versions">
			      </div>
			    </div>


			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">Rating</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. PG-13" type="text" name="rating">
				</div>
			      </div>


			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">Year</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. 1995" type="text" name="year">
				</div>
			      </div>


			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">Genre</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. drama" type="text" name="genre">
				</div>
			      </div>



			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">Aspect Ratio</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. 16:9" type="text" name="aspect">
				</div>
			      </div>



			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">UPC</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. 60000000" type="text" name="upc">
				</div>
			      </div>


			    <div class="control-group">
			      <label class="control-label" for=
				     "inputUser">DVD Release Date</label>

			      <div class="controls">
				<input id="inputUser" placeholder=
				       "E.g. 2003" type="text" name="dvdRelease">
				</div>
			      </div>

				* Required fields
                            <div class="control-group">
                              <div class="controls">
				<br>
				  <button class="btn btn-info" type="submit" name="submit">Send it</button>
				  <a type="button" class="btn btn-default" href="index.php">Back to store</a>
				</div>
                              </div>
			    </form>
			  </div>
			</div>
		      </div>
		    </div>
		  </div>
		</center>
';
	}


require("footer.php");

?>
