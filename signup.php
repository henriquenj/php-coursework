<?php

require("header.php");

// already logged users don't see this page
if ($user != NULL)
	{
		// we are logged, go to index page
		header('Location:index.php');
	}

// script to create new users

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
	{
		// handle the form if has been submitted
		
		// html for draw the errors
		$finalWarning ="<br><br><br><br><br><br><center><div class=\"row \"><div class=\"col-md-6 center-block \"><div class=\"alert alert-danger\">";

		$error = false;// set this true if we have at least one error
		$con =  DatabaseConnect();

		// copy to local variables
		$username = $_POST['username'];
		$password = $_POST['password'];
		$first = $_POST['first'];
		$surname = $_POST['surname'];
		$address = $_POST['address'];
		$town = $_POST['town'];
		$postcode = $_POST['postcode'];
		
		if (empty($username))
			{
				$finalWarning .= "You must type a username!<br>";
				$error = true;
			}
		else
			{
				// check if this username is already taken
				$result = mysqli_query($con,"SELECT username FROM users WHERE username = '$username';");
				$num = mysqli_num_rows($result);
				if ($num == 1)
					{
						$finalWarning .= "This username is already in use!<br>";
						$error = true;
					}
			}
		if (strlen($password) < 4)
			{
				$finalWarning .= "You must have a password of at least 4 characters!<br>";
				$error = true;
			}
		if (empty($first))
			{
				$finalWarning .= "You must provide at least you first name!<br>";
				$error = true;
			}

		if (!$error)
			{
				echo "<br><br><br><br><br><br><div class=\"row\"><center><div class=\"col-md-6 center-block\"><div class=\"alert alert-success\">Success, welcome $first.<br><br><a type=\"button\" class=\"btn btn-success\" href=\"index.php\">Continue</a>
</div></div><div></center>";
				// hash the password using the username as salt
				$hash = hash("sha256",$password . $username);
				$sessionID = session_id();
				session_destroy();
				// ok, cool, store on database
				mysqli_query($con,"INSERT INTO users (GivenName,username,hash,sessionID) VALUES ('$first','$username','$hash','$sessionID');");
				if (!empty($surname))
					{
						mysqli_query($con,"UPDATE users SET Surname = '$surname' WHERE username = '$username'");
					}
				if (!empty($town))
					{
						mysqli_query($con,"UPDATE users SET Town = '$town' WHERE username = '$username'");
					}
				if (!empty($address))
					{
						mysqli_query($con,"UPDATE users SET Address = '$address' WHERE username = '$username'");
					}
				if (!empty($postcode))
					{
						mysqli_query($con,"UPDATE users SET PostCode = '$postcode' WHERE username = '$username'");
					}
			}
		else
			{
				// put final piece
				$finalWarning .= "<br><a type=\"button\" class=\"btn btn-danger\" href=\"signup.php\">Go back</a></div></div><br></center></div>";
				// print error composed error message
				echo "$finalWarning";
			}

		DatabaseDisconnect($con);
		
	}

else
	{
		// output html form
		// heavily modified from this template 
		// http://bootsnipp.com/snippets/featured/parallel-signin-and-signup
		echo '
<br><br><br><br>
<center>
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="span6">
          <div class="area">
            <form class="form-horizontal" action="signup.php" method="post">
              <div class="heading">
                <h4 class="form-heading">Sign Up</h4>
              </div>


              <div class="control-group">
                <label class="control-label" for=
                       "inputUser">Username*</label>

                <div class="controls">
                  <input id="inputUser" placeholder=
                         "E.g. henry" type="text" name="username" required>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label" for=
			 "inputPassword">Password*</label>

                  <div class="controls">
                    <input id="inputPassword" placeholder=
                           "Min. 4 Characters" type="password" name="password" required>
                    </div>
                  </div>

		<div class="control-group">
                  <label class="control-label" for="inputFirst">First
                    Name*</label>

                  <div class="controls">
                    <input id="inputFirst" placeholder=
                           "E.g. Henrique" type="text" name="first" required>
                    </div>
                  </div>

                  <div class="control-group">
                    <label class="control-label" for="inputLast">Last
                      Name</label>

                    <div class="controls">
                      <input id="inputLast" placeholder="E.g. Jung"
                             type="text" name="surname">
                      </div>
                    </div>

		      <div class="control-group">
			<label class="control-label" for=
			       "inputUser">Address</label>

			<div class="controls">
			  <input id="inputUser" placeholder=
				 "E.g. Lewisham Way" type="text" name="address">
			  </div>
			</div>


			<div class="control-group">
			  <label class="control-label" for=
				 "inputUser">Town</label>

			  <div class="controls">
			    <input id="inputUser" placeholder=
				   "E.g. London" type="text" name="town">
			    </div>
			  </div>


			  <div class="control-group">
			    <label class="control-label" for=
				   "inputUser">Postcode</label>

			    <div class="controls">
			      <input id="inputUser" placeholder=
				     "E.g. SE14 6NW" type="text" name="postcode">
			      </div>
			    </div>
				* Required fields
                            <div class="control-group">
                              <div class="controls">
				<br>
				  <button class="btn btn-success" type="submit" name="submit">Sign Up</button>
				  <a type="button" class="btn btn-default" href="login.php">Back to login</a>
				</div>
                              </div>
			    </form>
			  </div>
			</div>
		      </div>
		    </div>
		  </div>
		</center>
		';
	}

require("footer.php");

?>
