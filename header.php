<?php

require_once("database.php");
$user = NULL;
// check if the user is logged
session_start();
$session = session_id();
	
// look in the database which user has this seassion
$con = DatabaseConnect();
$result = mysqli_query($con,"SELECT username FROM users WHERE sessionID = '$session';");
$obj = mysqli_fetch_object($result);
if ($obj != NULL)
	{
		// update user name
		$user = $obj->username;

	}

?>		

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	  <title>Henrique Jung</title>

	  <!-- Bootstrap -->
	  <!-- Bootstrap core CSS -->
	  <link href="css/bootstrap.min.css" rel="stylesheet">
	    <!-- Bootstrap theme -->
	    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
	      
	      <link href="style.css" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		    <![endif]-->
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->

		<!-- here ends bootstrap inital sequence -->

	      </head>
	      <body>
		<div class="container">

 <!-- here comes the menu portion -->
<div class="navbar navbar-inverse navbar-fixed-top navbar-collapse" role="navigation">
  <div class="container">
    <div class="container">
      <div class="navbar-header">
	<ul class="navbar-brand">Store System</ul>
      </div>
      <ul class="nav navbar-nav navbar-right">
<?php
//write name of the user if logged
			 if ($user != NULL)
				 {
					 echo "You are logged as $user    <a type=\"button \"class=\"btn btn-warning\" href=\"logout.php\">Logout</a>";
				 }
			 else
				 {
					 echo "You are not logged";
				 }
?>
      </ul>
    </div>
  </div>
</div>
